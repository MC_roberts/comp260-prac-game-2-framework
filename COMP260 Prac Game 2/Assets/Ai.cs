﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Ai : MonoBehaviour {
	public float force = 10f;
	public GameObject Puck;
	public GameObject goal;


	// Use this for initialization
	private Rigidbody rigidbody;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

	}

	// Update is called once per frame
	void Update () {


	}





	void FixedUpdate () {
		Vector3 pos = (goal.transform.position + Puck.transform.position)/2;
		Vector3 dir = pos - rigidbody.position;

		rigidbody.velocity = dir;
	}
}

