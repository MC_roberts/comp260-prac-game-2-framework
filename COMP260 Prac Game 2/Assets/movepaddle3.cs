﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class movepaddle3 : MonoBehaviour {


		public float force = 10f;

		// Use this for initialization
		private Rigidbody rigidbody;
		private Vector3 direction;

		void Start () {
			rigidbody = GetComponent<Rigidbody>();
			rigidbody.useGravity = false;

		}

		// Update is called once per frame
		void Update () {
			direction.y = Input.GetAxis ("Vertical");
			direction.x = Input.GetAxis("Horizontal");


		}




		void FixedUpdate () {



			rigidbody.velocity = (direction * force);
		}
	}

